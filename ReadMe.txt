Spring Boot Example
===================

This example shows how to work with the simple Camel application based on the Spring Boot.

The example generates messages using timer trigger, writes them to a Kafka topic.

Another route reads the messages from the Kafka topic.

In order to run this example you need to install or point to an existing Kafka server.

The messages are send as an serialized avro object.
You will need to compile this example first:
  mvn install

To run the example type
  mvn spring-boot:run

You can also execute the fat WAR directly:

  java -jar target/KafkaTestProject-1.0-SNAPSHOT.war

You will see the message printed to the console every second.

To stop the example hit ctrl + c


