package nl.janssen.kafka.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Created by janssendj on 18-10-16.
 */
@Component
public class GenerateUUID {


    public String getGuid(){
        String uuid = UUID.randomUUID().toString();
        return uuid;
    }
}
