package nl.janssen.kafka.utils;

import example.avro.User;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.commons.lang3.SerializationUtils;

import java.io.*;
import java.util.Map;

/**
 * Created by janssendj on 24-10-16.
 */
public class AvroSerializer implements Serializer{


    public void configure(Map map, boolean b) {

    }

    public byte[] serialize(String s, Object object) {
        User user = (User) object;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(out, null);
        DatumWriter<User> writer = new SpecificDatumWriter<User>(User.getClassSchema());

        try {
            writer.write(user, encoder);
            encoder.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return  out.toByteArray();
    }

    public void close() {

    }
}
