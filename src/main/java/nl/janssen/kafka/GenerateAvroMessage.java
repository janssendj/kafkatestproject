package nl.janssen.kafka;

import example.avro.User;
import org.springframework.stereotype.Component;

/**
 * Created by janssendj on 19-10-16.
 */
@Component
public class GenerateAvroMessage {

    public User createAvroExampleMessage(){
        User user = new User();
        user.setName("nameIsBlaat");
        user.setFavoriteColor("blue");
        user.setFavoriteNumber(123);

        return user;
    }

}
