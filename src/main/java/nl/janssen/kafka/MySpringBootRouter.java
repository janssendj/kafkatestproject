package nl.janssen.kafka;

import example.avro.User;
import org.apache.camel.LoggingLevel;
import org.apache.camel.component.kafka.KafkaConstants;
import org.apache.camel.dataformat.avro.AvroDataFormat;
import org.apache.camel.spi.ShutdownStrategy;
import org.apache.camel.spring.boot.FatJarRouter;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.UUID;

@SpringBootApplication
public class MySpringBootRouter extends FatJarRouter {


    @Override
    public void configure() {

        AvroDataFormat format = new AvroDataFormat(User.getClassSchema());


        from("timer:trigger").routeId("producer")
                .transform().simple("bean:generateAvroMessage?method=createAvroExampleMessage")
                .setHeader(KafkaConstants.KEY,simple("bean:generateUUID?method=getGuid") )
                .log("${body}")
                .to("kafka:localhost:9092?topic=rubix&serializerClass=nl.janssen.kafka.utils.AvroSerializer");

        from("kafka:localhost:9092?topic=rubix&groupId=testing&consumersCount=1").routeId("Consumer")
                .log("${body}")
                .unmarshal(format)
                .log("${body.name}")
                .end();
    }




}
